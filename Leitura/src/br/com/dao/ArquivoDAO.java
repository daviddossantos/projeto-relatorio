/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dao;

import br.com.model.Arquivo;
import br.com.connection.ConnectionFactory;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.management.Query;

/**
 *
 * @author david
 */
public class ArquivoDAO {

    public void inserir(Arquivo c) throws IOException {

        Connection conexao = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;

        try {

            stmt = conexao.prepareStatement("INSERT INTO leitura(NOMEARQUIVO, DATAPROCESSAMENTO, HORARIOPROCESSAMENTO, STATUSARQUIVO)"
                    + "VALUES(?,?,?,?)");

            stmt.setString(1, c.getNome());
            stmt.setString(2, c.getData());
            stmt.setString(3, c.getHorario());
            stmt.setString(4, String.valueOf(c.getStatus()));

            stmt.executeUpdate();

        } catch (SQLException e) {

            throw new RuntimeException("Ocorreu um erro: " + e);
        }

    }

    public boolean verificar(String dataIni, String dataFinal) throws IOException {

        Connection conexao = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        boolean resultado = false;

        try {

            stmt = conexao.prepareStatement("SELECT * FROM leitura WHERE DATAPROCESSAMENTO BETWEEN  ? AND ? ");
            stmt.setString(1, dataIni);
            stmt.setString(2, dataFinal);

            rs = stmt.executeQuery();

            while (rs.next()) {

                return resultado = true;

            }

            return resultado;

        } catch (Exception e) {
            throw new RuntimeException("Ocorreu um erro: " + e);
        }

    }

}
