/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.main;

import br.com.model.Arquivo;
import br.com.dao.ArquivoDAO;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author dcruz
 */
public class LeituraArquivos {

    static int cont_erros = 0;
    static int cont_cadastros = 0;
    static SimpleDateFormat dataFormatada = new SimpleDateFormat("dd/MM/yyyy");

    public static void main(String[] args) throws IOException {

        int delay = 3000;
        int intervalo = 1000;

        Timer temporizador = new Timer();

        temporizador.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {

                try {

                    criarDiretorios();

                    List<String> diretoriosLidos = obter_diretorios();

                    verifica_arquivos(diretoriosLidos);

                    gerar_status();

                } catch (IOException e) {

                    throw new RuntimeException(e);
                }
            }

        }, delay, intervalo);

    }

    static void criarDiretorios() throws IOException {

        Properties props = getProperties();

        String CADASTRO_CLIENTE = props.getProperty("CADASTRO_CLIENTE");
        String CADASTRO_PENDENTES = props.getProperty("CADASTRO_PENDENTES");
        String CADASTROS = props.getProperty("CADASTROS");
        String ERROS = props.getProperty("ERROS");

        boolean verificar = Files.exists(Paths.get(CADASTRO_CLIENTE));

        if (verificar == false) {

            Files.createDirectory(Paths.get(CADASTRO_CLIENTE));
            Files.createDirectory(Paths.get(CADASTRO_PENDENTES));
            Files.createDirectory(Paths.get(CADASTROS));
            Files.createDirectory(Paths.get(ERROS));

        }

    }

    static List<String> obter_diretorios() throws IOException {

        Properties props = getProperties();

        String CADASTRO_PENDENTES = props.getProperty("CADASTRO_PENDENTES");
        String ERROS = props.getProperty("ERROS");

        
        Path converter_erros = Paths.get(ERROS);

        //array para gravar os caminho dos arquivos que estão
        //no diretorio "Cadastro_Pendentes"
        List<String> diretorio_gravados = new ArrayList<>();

        List<String> diretorio_corretos = new ArrayList<>();
        List<String> diretorio_erros = new ArrayList<>();

        DirectoryStream<Path> diretorio = Files.newDirectoryStream(Paths.get(CADASTRO_PENDENTES));

        //verificar todos os arquivos que estão no diretorio "Cadastro_Pendentes"
        for (Path cadastro_pendentes : diretorio) {

            //e feito a conversão dos diretorios para String
            String diretorio_lidos = String.valueOf(cadastro_pendentes);

            boolean formato = diretorio_lidos.endsWith(".txt");

            if (formato == false) {

                inserir_erro(cadastro_pendentes);

                //mover o arquivo para o pasta "erros"
                Files.move(cadastro_pendentes, converter_erros.resolve(cadastro_pendentes.getFileName()));

                diretorio_corretos.add(diretorio_lidos);

            } else {

                //armazenar diretorios dos arquivos que estão 'corretos'
                diretorio_gravados.add(diretorio_lidos);

                diretorio_erros.add(diretorio_lidos);

            }

        }

        cont_cadastros = diretorio_corretos.size();
        cont_erros = diretorio_erros.size();

        return diretorio_gravados;
    }

    static void verifica_arquivos(List<String> diretoriosLidos) throws IOException {

        String DiretorioConvertido = null;
        Path diretorioLido = null;
        int posicao = -1, contador_erros = 0, contador_corretos = 0;

        //array criado para verificar se os nomes estão corretos
        String formatacao[] = {"Codigo", "Nome", "Sexo", "Idade", "Cargo"};

        //Lista que armazenar o diretorio dos arquivos fora
        //da formataçao solicitada
        List<String> diretorio_erros = new ArrayList<>();

        //Lista que armazenar o diretorio dos arquivos que estão corretos
        List<String> diretorio_corretos = new ArrayList<>();

        for (String caminhosLidos : diretoriosLidos) {

            diretorioLido = Paths.get(caminhosLidos);

            DiretorioConvertido = diretorioLido.toString();

            List<String> texto = Files.readAllLines(diretorioLido);

            for (String cliente : texto) {

                posicao++;

                if (posicao > 4) {

                    posicao = 0;
                }

                String[] verificador = cliente.split(":");

                if (" ".equals(verificador[1]) || !formatacao[posicao].equals(verificador[0])) {

                    contador_erros++;

                } else {

                    contador_corretos++;

                }

            }
        }

        if (contador_erros >= 1) {

            //inseri um registro no banco
            inserir_erro(diretorioLido);

            diretorio_erros.add(DiretorioConvertido);

        } else if (contador_corretos > 4) {

            //inseri um registro no banco
            inserir_correto(diretorioLido);

            diretorio_corretos.add(DiretorioConvertido);
        }

        mover_erros(diretorio_erros);
        mover_corretos(diretorio_corretos);
    }

    static void mover_erros(List<String> diretorio_erros) throws IOException {

        Properties props = getProperties();

        String ERRO = props.getProperty("ERROS");
        Path converter_erro = Paths.get(ERRO);

        for (String arquivo_erro : diretorio_erros) {

            Path arquivo_com_erro = Paths.get(arquivo_erro);

            //move os arquivos para a pasta "Erros"
            Files.move(arquivo_com_erro, converter_erro.resolve(arquivo_com_erro.getFileName()));

        }

    }

    static void mover_corretos(List<String> diretorio_corretos) throws IOException {

        Properties props = getProperties();

        String CADASTRO = props.getProperty("CADASTROS");
        Path converter_cadastro = Paths.get(CADASTRO);

        for (String arquivo_correto : diretorio_corretos) {

            Path arquivo_esta_correto = Paths.get(arquivo_correto);

            imprimir_informaçoes(arquivo_esta_correto);

            //mover o arquivo para o pasta "Cadastro"
            Files.move(arquivo_esta_correto, converter_cadastro.resolve(arquivo_esta_correto.getFileName()));

        }

    }

    static void imprimir_informaçoes(Path arquivo_correto) throws IOException {

        String arquivo_diferente = null;

        List<String> dados = Files.readAllLines(arquivo_correto);

        //imprimir dados do cliente que estão na pasta "Cadastros"
        for (String cliente : dados) {

            if (!arquivo_correto.toString().equals(arquivo_diferente)) {

                arquivo_diferente = arquivo_correto.toString();

                //inserir um espaço entre cada arquivo lido
                System.out.println(" ");

            }
            //imprimir no console os cliente que estão na pasta "Cadastro"
            System.out.println(cliente);
        }

    }

    static Properties getProperties() throws FileNotFoundException, IOException {

        Properties props = new Properties();

        FileInputStream file = new FileInputStream(
                "src\\br\\com\\main\\configuracao.properties");

        props.load(file);

        return props;

    }

    static void gerar_status() throws IOException {

        Date data = new Date();
        SimpleDateFormat horarioFormatado = new SimpleDateFormat("HH:mm:ss");

        List<String> dados = new ArrayList<>();

        Properties props = getProperties();

        String STATUS = props.getProperty("STATUS");

        Path status = Paths.get(STATUS);

        boolean verificar = Files.exists(status);

        if (verificar == false) {

            Files.createFile(status);

            dados.add("-----------------------------");
            dados.add("Data do Último processamento");
            dados.add("-----------------------------");
            dados.add("Data: " + dataFormatada.format(data));
            dados.add("Horário: " + horarioFormatado.format(data));
            dados.add("Arquivos processados: " + cont_cadastros);
            dados.add("Erros: " + cont_erros);
            dados.add(" ");
            dados.add("-----------------------------");
            dados.add("Dados Totais: ");
            dados.add("-----------------------------");
            dados.add("Clientes cadastrados: " + cont_cadastros());
            dados.add("Total de erros: " + cont_erros());

            Files.write(status, dados);

        }

        if (cont_cadastros > 0 || cont_erros > 0) {

            dados.add("-----------------------------");
            dados.add("Data do Último processamento");
            dados.add("-----------------------------");
            dados.add("Data: " + dataFormatada.format(data));
            dados.add("Horário: " + horarioFormatado.format(data));
            dados.add("Arquivos processados: " + cont_cadastros);
            dados.add("Erros: " + cont_erros);
            dados.add(" ");
            dados.add("-----------------------------");
            dados.add("Dados Totais: ");
            dados.add("-----------------------------");
            dados.add("Clientes cadastrados: " + cont_cadastros());
            dados.add("Total de erros: " + cont_erros());

            Files.write(status, dados);

        } else {

            List<String> linha = Files.readAllLines(status);

            linha.remove(11);
            linha.add(11, "Clientes cadastrados: " + cont_cadastros());

            linha.remove(12);
            linha.add(12, "Total de erros: " + cont_erros());

            Files.write(status, linha);

        }

    }

    public static int cont_cadastros() throws IOException, IOException {

        int contador = 0;

        Properties props = getProperties();

        String CADASTROS = props.getProperty("CADASTROS");

        DirectoryStream<Path> cadastros = Files.newDirectoryStream(Paths.get(CADASTROS));

        for (Path arquivos_correto : cadastros) {
            contador++;
        }

        return contador;
    }

    public static int cont_erros() throws IOException {

        Properties props = getProperties();

        String ERROS = props.getProperty("ERROS");

        DirectoryStream<Path> erros = Files.newDirectoryStream(Paths.get(ERROS));

        int contador = 0;

        for (Path arquivo_erro : erros) {
            contador++;
        }

        return contador;
    }

    static void inserir_erro(Path nome) throws IOException {

        Date data = new Date();
        SimpleDateFormat horarioFormatado = new SimpleDateFormat("HH:mm:ss");

        Arquivo a = new Arquivo();
        ArquivoDAO dao = new ArquivoDAO();

        a.setNome(nome.getFileName().toString());
        a.setHorario(horarioFormatado.format(data));
        a.setData(dataFormatada.format(data));
        a.setStatus("ERRO");

        dao.inserir(a);

    }

    static void inserir_correto(Path nome) throws IOException {

        Date data = new Date();
        SimpleDateFormat horarioFormatado = new SimpleDateFormat("HH:mm:ss");

        Arquivo a = new Arquivo();
        ArquivoDAO dao = new ArquivoDAO();

        a.setNome(nome.getFileName().toString());
        a.setHorario(horarioFormatado.format(data));
        a.setData(dataFormatada.format(data));
        a.setStatus("CORRETO");

        dao.inserir(a);

    }
}
