/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.connection;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

/**
 *
 * @author david
 */
public class ConnectionFactory {
    
    
   

    public static Connection getConnection() throws IOException {

        Properties props = getProperties();

        final String URL = props.getProperty("URL");
        final String PASS = props.getProperty("PASS");
        final String USER = props.getProperty("USER");

        try {

            return DriverManager.getConnection(URL, USER, PASS);

        } catch (SQLException e) {

            throw new RuntimeException("Ocorreu um erro: " + e);
        }

    }

    public static void closeConnection(Connection con) throws SQLException {

        try {

            if (con != null) {

                con.close();
            }

        } catch (SQLException e) {

            throw new RuntimeException("Ocorreu um erro: " + e);
        }

    }

    public static void closeConnection(Connection con, PreparedStatement stmt) throws SQLException {

        closeConnection(con);

        try {

            if (stmt != null) {

                stmt.close();
            }

        } catch (SQLException e) {

            throw new RuntimeException("Ocorreu um erro: " + e);
        }

    }

    public static void closeConnection(Connection con, PreparedStatement stmt, ResultSet rs) throws SQLException {

        closeConnection(con, stmt);

        try {

            if (rs != null) {

                rs.close();
            }

        } catch (SQLException e) {

            throw new RuntimeException("Ocorreu um erro: " + e);
        }

    }

    public static Properties getProperties() throws FileNotFoundException, IOException {

        Properties props = new Properties();

        FileInputStream file = new FileInputStream(
                "src\\br\\com\\main\\configuracao.properties");
        props.load(file);

        return props;

    }
}
