/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package definicoes_relatorios;

import br.com.connection.ConnectionFactory;
import br.com.dao.ArquivoDAO;
import br.com.model.Arquivo;
import br.com.view.GerarRelatorio;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author david
 */
public class Gerar_Relatorio {

    public static void gerar(String dataIni, String dataFinal) throws JRException, IOException, SQLException {

        //compilar relatorio para o formato Jasper
        String relatorio = JasperCompileManager.compileReportToFile("src\\definicoes_relatorios\\relatorio_david.jrxml");

        if ("  /  /    ".equals(dataFinal)) {

            dataFinal = dataIni;

        }
        
        ArquivoDAO dao = new ArquivoDAO();
        boolean resultado = dao.verificar(dataIni, dataFinal);
                        

        if (resultado == true) {

          
            //parametros que serão utilizados para filtrar o relatorio,
            //no caso 'data inicial' e 'data final'
            Map<String, Object> parametros = new HashMap<String, Object>();
            parametros.put("DATA_INI", dataIni);
            parametros.put("DATA_FINAL", dataFinal);

            //obtém conexão 
            Connection conexao = ConnectionFactory.getConnection();

            //gerar o relatorio
            JasperPrint jasperPrint = JasperFillManager.fillReport(relatorio, parametros, conexao);

            //exportar para PDF
//            JRExporter exportar = new JRPdfExporter();
//            exportar.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
//            exportar.setParameter(JRExporterParameter.OUTPUT_FILE, new File("C:\\Users\\david\\Desktop\\teste.pdf"));
//            exportar.exportReport();
            
            //JasperViewer - será aberto a ferramenta do Jasper,
            //onde e possivel visualizar o relatorio e escolher um tipo de 
            //formato a ser salvo o mesmo.
            JasperViewer.viewReport(jasperPrint, false);
            
     

            //fecha conexao
            ConnectionFactory.closeConnection(conexao);

        } else {

            JOptionPane.showMessageDialog(null, "Nenhum registro localizado! Informe outra DATA");

        }

    }

}
